import bleach
from fastapi import APIRouter, Body
from pony.orm import db_session, commit

from .db_models import User
from .pydantic_models import UserPresentation

router = APIRouter()


@router.get('/user/{uid:int}')
async def user_get(uid: int):
    with db_session:
        user = User[uid]
    result = UserPresentation.from_orm(user)
    return result


@router.patch('/user/{uid:int}')
async def user_patch(uid: int, body: str = Body("")):
    with db_session:
        user = User[uid]
        user.bio = bleach.clean(body)
        commit()
    result = UserPresentation.from_orm(user)
    return result
