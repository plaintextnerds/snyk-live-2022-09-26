from pony.orm import Database, PrimaryKey, Required, Optional

db = Database()


class User(db.Entity):
    uid = PrimaryKey(int, auto=True)
    username = Required(str, unique=True)
    password = Required(str)
    bio = Optional(str, nullable=True)
    is_admin = Required(bool, default=False)
