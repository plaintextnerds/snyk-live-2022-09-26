from pydantic import BaseModel


class UserPresentation(BaseModel):
    uid: int
    username: str
    bio: str

    class Config:
        orm_mode = True
