from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.templating import Jinja2Templates
from pony.orm import db_session
from starlette.requests import Request
from starlette.responses import HTMLResponse

from app.db_models import db, User
from app.api import router as api_router

db.bind(provider='sqlite', filename='database.sqlite', create_db=True)
db.generate_mapping(create_tables=True)


origins = [
    "http://127.0.0.1:8000",
    "http://localhost:8000"
]

fastapi = FastAPI()
fastapi.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "PATCH"],
    allow_headers=["*"],
)
fastapi.include_router(api_router, prefix="/api")

templates = Jinja2Templates(directory="templates")

@fastapi.get("/", response_class=HTMLResponse)
async def index(request: Request,):
    with db_session:
        user = User[1]
    return templates.TemplateResponse("index.html", {'request': request, 'user': user})
